import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/employee',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../components/HelloWorld.vue')
  },
  {
    path: '/employee/:id',
    name: 'EmployeePage',
    component: () => import(/* webpackChunkName: "about" */ '../components/employeePage.vue')
  },
  {
    path: '/create',
    name: 'userCreate',
    component: () => import(/* webpackChunkName: "about" */ '../components/userCreate.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
